Pod::Spec.new do |s|
  s.name = "iossdk"
  s.version = "0.5.0"
  s.summary = "SDK for scanning Light device"
  s.license = "Commercial"
  s.authors = {"TaiHua Wu"=>"robert.wu@lig.com.tw", "Plain Wu"=>"plain.wu@lig.com.tw"}
  s.homepage = "https://www.lig.com.tw"
  s.description = "This SDK is used to scanning Light devices produced by Light Generation Ltd.,"
  s.frameworks = ["AssetsLibrary", "CoreMedia", "CoreImage", "CoreVideo", "AVFoundation", "CoreAudio"]

  s.pod_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'TARGETED_DEVICE_FAMILY' => 1,
    'PRODUCT_BUNDLE_IDENTIFIER' => 'tw.com.lig.sdk.lightsdk'
  }
  s.user_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'ENABLE_BITCODE' => 'NO',
    'OTHER_LDFLAGS' => '-lObjC',
    'TARGETED_DEVICE_FAMILY' => 1,
    'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'
  }
  s.requires_arc = true
  s.source = { http: 'https://dl.lig.com.tw/iossdk/iossdk-0.5.0.zip' }
  s.static_framework = true

  s.ios.deployment_target    = '11.3'
  #s.ios.vendored_framework   = 'ios/iossdk.embeddedframework/iossdk.framework'
  s.ios.vendored_framework   = 'iossdk.framework'

  s.dependency 'AFNetworking'
  s.dependency 'OpenCV', '~> 3.4.6'
end
