# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#
# Resource https://github.com/magicien/GLTFSceneKit

Pod::Spec.new do |s|
  s.name             = "LigGltfSceneKit"
  s.version          = "1.0.2"
  s.summary          = "glTF loader for SceneKit"

  s.description      = <<-DESC
  glTF loader for SceneKit
                       DESC

  s.homepage         = 'https://gitlab.com/lig_dev/LigGltfSceneKit'
  s.license          = "Commercial"
  s.author           = { 'Plain Wu' => 'plain.wu@lig.com.tw' }
  s.source           = { :git => 'https://gitlab.com/lig_dev/LigGltfSceneKit.git', :tag => s.version.to_s }
  s.ios.deployment_target = "13.0"
  s.osx.deployment_target = "10.13"
  s.platform = :ios, '13'
  s.swift_version = '5.0'
  s.source_files = "Sources/**/*.swift"
  s.resources = "Sources/**/*.shader"
  
  s.requires_arc = true
  s.pod_target_xcconfig = {
    "SWIFT_VERSION" => "5.0",
    "SWIFT_ACTIVE_COMPILATION_CONDITIONS" => "SEEMS_TO_HAVE_VALIDATE_VERTEX_ATTRIBUTE_BUG SEEMS_TO_HAVE_PNG_LOADING_BUG"
  }
end
