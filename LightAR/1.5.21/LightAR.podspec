Pod::Spec.new do |s|
  s.name             = 'LightAR'
  s.module_name      = "LightAR"
  s.version          = '1.5.21'
  s.summary          = 'Basic AR service'
  s.description      = <<-DESC
Basic AR service
                       DESC
  s.homepage         = 'https://www.lig.com.tw/'
  s.license          = "Commercial"
  s.author           = { 'RobertWu' => 'robert.wu@lig.com.tw', 'Plain Wu' => 'plain.wu@lig.com.tw' }
  
  s.source           = { git: "git@gitlab.com:lig_dev/lightar.git", tag: s.version.to_s}
  s.ios.deployment_target = '13'
  s.platform = :ios, '13'
  s.swift_version = '5.0'
  s.requires_arc = true
  s.source_files = 'LightAR/Classes/**/*'
  s.dependency "Kingfisher" , '5.15.8'
  s.dependency 'LigGltfSceneKit'
  s.frameworks = 'WebKit'
  s.resources = [
    'LightAR/Assets/**/*'
  ]
end
