Pod::Spec.new do |s|
  s.name             = 'LightGeneral'
  s.module_name      = "LightGeneral"
  s.version          = '1.0.4'
  s.summary          = 'General function pod'
  s.description      = <<-DESC
Basic function collection
                       DESC
  s.homepage         = 'https://gitlab.com/RobertLig/lightgeneral'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'RobertWu' => 'robert.wu@lig.com.tw' }
  s.source           = { :git => 'git@gitlab.com:lig_ios/lightgeneral.git', :tag => s.version.to_s }
  s.ios.deployment_target = '13'
  s.platform = :ios, '13'
  s.subspec 'Extension' do |ss|
      ss.source_files = 'LightGeneral/Classes/Extension/**/*'
      ss.dependency "Kingfisher"
  end
  s.subspec 'Model' do |ss|
      ss.dependency 'LightGeneral/Extension'
      ss.source_files = 'LightGeneral/Classes/Model/**/*'
  end
  s.subspec 'View' do |ss|
      ss.dependency 'LightGeneral/Extension'
      ss.dependency 'LightGeneral/Model'
      ss.source_files = 'LightGeneral/Classes/View/**/*'
  end
  s.subspec 'ViewController' do |ss|
      ss.dependency 'LightGeneral/View'
      ss.source_files = 'LightGeneral/Classes/ViewController/**/*'
  end
  s.swift_version = '5.0'
  s.requires_arc = true
  s.frameworks = 'UIKit'
end
