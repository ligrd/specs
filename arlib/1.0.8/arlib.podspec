Pod::Spec.new do |s|
  s.name             = 'arlib'
  s.version          = '1.0.8'
  s.summary          = 'SDK for rendering AR Objects configured by Light Space' 
  s.description      = <<-DESC
This SDK is used to scanning Light devices produced by Light Generation Ltd.,
                       DESC

  s.homepage         = 'https://www.lig.com.tw'
  s.license          = { type: 'Commercial' }
  s.author           = { 'Plain Wu' => 'plain.wu@lig.com.tw' }

  s.pod_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'TARGETED_DEVICE_FAMILY' => 1,
    'PRODUCT_BUNDLE_IDENTIFIER' => 'tw.com.lig.sdk.arlib'
  }
  s.user_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'ENABLE_BITCODE' => 'NO',
    'OTHER_LDFLAGS' => ['$(inherited)', '-lObjC', '-l"c++"'],
    'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'
  }

  s.platform = :ios
  s.ios.deployment_target = '13.1'
  s.dependency 'Kingfisher', '7.2.0'
  s.dependency 'LigGltfSceneKit', '1.0.2'
  s.dependency 'ReachabilitySwift', '5.0.0'
  s.dependency 'driver', '2.2.62'
  s.frameworks = 'CoreImage', 'CoreVideo', 'AVFoundation'
  s.source = { http: "https://dl.lig.com.tw/iossdk/arlib-#{s.version}.zip" }
  s.ios.vendored_framework = 'arlib.framework'
end
