Pod::Spec.new do |s|
  s.name = "ligsdk"
  s.version = "2.2.34"
  s.summary = "SDK for scanning Light device"
  s.license = "Commercial"
  s.authors = {"TaiHua Wu"=>"robert.wu@lig.com.tw", "Plain Wu"=>"plain.wu@lig.com.tw"}
  s.homepage = "https://www.lig.com.tw"
  s.description = "This SDK is used to scanning Light devices produced by Light Generation Ltd.,"

  s.platform = :ios
  s.ios.deployment_target = '11.3'
  s.frameworks = 'CoreImage', 'CoreVideo', 'AVFoundation', 'AFNetworking'

  s.pod_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'TARGETED_DEVICE_FAMILY' => 1,
    'PRODUCT_BUNDLE_IDENTIFIER' => 'tw.com.lig.sdk.lightsdk'
  }
  s.user_target_xcconfig = {
    'ARCHS' => '$(ARCHS_STANDARD_64_BIT)',
    'VALID_ARCHS' => 'arm64 arm64e',
    'SDKROOT' => 'iphoneos',
    'ENABLE_BITCODE' => 'NO',
    'OTHER_LDFLAGS' => ['$(inherited)', '-lObjC', '-l"c++"'],
    'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'
  }
  s.requires_arc = true
  s.static_framework = true

  s.dependency 'AFNetworking/Reachability', ' ~> 3.0'
  s.dependency 'AFNetworking/Serialization', ' ~> 3.0'
  s.dependency 'AFNetworking/Security', ' ~> 3.0'
  s.dependency 'AFNetworking/NSURLSession', ' ~> 3.0'

  s.source = { http: "https://dl.lig.com.tw/iossdk/iossdk-#{s.version}.zip" }
  s.ios.vendored_framework   = 'ligsdk.framework'
end
